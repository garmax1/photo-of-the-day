package co.garmax.demos.photooftheday;

import android.app.Application;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Parcel;
import android.test.ApplicationTestCase;
import android.util.Log;

import junit.framework.Assert;

import java.io.ByteArrayInputStream;
import java.util.List;

import co.garmax.demos.photooftheday.data.Photo;
import co.garmax.demos.photooftheday.data.PhotoDatabaseHelper;
import co.garmax.demos.photooftheday.data.PhotoXmlParser;

public class ApplicationTest extends ApplicationTestCase<Application> {
    private static final String TAG = "ApplicationTest";
    private Application mApplication;

    public ApplicationTest() {
        super(Application.class);
    }

    protected void setUp() throws Exception {
        super.setUp();
        createApplication();
        mApplication = getApplication();
    }

    private void assertPhotos(Photo photo1, Photo photo2) {
        Assert.assertEquals(photo1.getAuthor(), photo2.getAuthor());
        Assert.assertEquals(photo1.getTitle(), photo2.getTitle());
        Assert.assertEquals(photo1.getUrl(), photo2.getUrl());
    }

    public void testReadWritePhotoDatabase() throws Exception {
        PhotoDatabaseHelper databaseHelper = PhotoDatabaseHelper.getInstance(mApplication);
        SQLiteDatabase db = databaseHelper.getWritableDatabase();

        // Clear database
        databaseHelper.onUpgrade(db, 0, 1);

        // Create test object
        Photo photoWrite1 = new Photo("title", "author", "http1");
        Photo photoWrite2 = new Photo("title", "author", "http");

        // Write photo
        PhotoDatabaseHelper.insertPhoto(db, photoWrite1);
        PhotoDatabaseHelper.insertPhoto(db, photoWrite2);

        db.close();

        // Read photo
        Cursor photoCursor = databaseHelper.getPhotoCursor();

        Assert.assertEquals(photoCursor.getCount(), 2);

        photoCursor.close();
    }

    public void testPhotoSerialization() {
        Photo photoSrc = new Photo("title", "author", "http");

        // Serialize
        Parcel parcel = Parcel.obtain();
        photoSrc.writeToParcel(parcel, 0);

        // Reset for reading
        parcel.setDataPosition(0);

        //Deserialize
        Photo photoDest = Photo.CREATOR.createFromParcel(parcel);
        assertPhotos(photoSrc, photoDest);
    }

    public void testXMLPhotoParser() {
        // Parse xml
        List<Photo> photoList = null;
        try {
            photoList = new PhotoXmlParser().parse(new ByteArrayInputStream(XML_TEST.getBytes("UTF-8")));
        } catch (Exception e) {
            Log.e(TAG, "Exception when convert xml string");
        }

        Assert.assertNotNull(photoList);
        Assert.assertEquals(photoList.size(), 2);

        Photo photoParse1 = photoList.get(0);
        Photo photoParse2 = photoList.get(1);

        Assert.assertEquals(photoParse1.getAuthor(), "loveangelus");
        Assert.assertEquals(photoParse1.getTitle(), "C04O0322.jpg");
        Assert.assertEquals(photoParse1.getUrl(), "https://img-fotki.yandex.ru/get/15565/327456462.52/0_142ecf_c5b10b4d_XL");

        Assert.assertEquals(photoParse2.getAuthor(), "makkar1961");
        Assert.assertEquals(photoParse2.getTitle(), ".jpg");
        Assert.assertEquals(photoParse2.getUrl(), "https://img-fotki.yandex.ru/get/4123/328427050.0/0_1d0310_3ac319a4_XL");
    }

    private static final String XML_TEST = "" +
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<rss xmlns:atom=\"http://www.w3.org/2005/Atom\" xmlns:media=\"http://search.yahoo.com/mrss\" version=\"2.0\">\n" +
            "   <channel>\n" +
            "      <title>Все фото дня на Яндекс.Фотках</title>\n" +
            "      <atom:link rel=\"next\" href=\"http://fotki.yandex.ru/calendar/rss2?p=1\" />\n" +
            "      <link>http://fotki.yandex.ru/calendar/</link>\n" +
            "      <pubDate>Mon, 10 Aug 2015 19:57 +0300</pubDate>\n" +
            "      <lastBuildDate>Mon, 10 Aug 2015 19:57 +0300</lastBuildDate>\n" +
            "      <atom:icon>http://fotki.yandex.ru/i/bird.png</atom:icon>\n" +
            "      <item>\n" +
            "         <author>loveangelus</author>\n" +
            "         <link>http://fotki.yandex.ru/users/loveangelus/view/1322703/</link>\n" +
            "         <media:thumbnail url=\"https://img-fotki.yandex.ru/get/15565/327456462.52/0_142ecf_c5b10b4d_M\" medium=\"image\" type=\"image/jpeg\" />\n" +
            "         <media:content url=\"https://img-fotki.yandex.ru/get/15565/327456462.52/0_142ecf_c5b10b4d_XL\" medium=\"image\" type=\"image/jpeg\" />\n" +
            "         <pubDate>Mon, 10 Aug 2015 19:57 +0300</pubDate>\n" +
            "         <title>C04O0322.jpg</title>\n" +
            "         <description>&lt;p&gt;&lt;a href=\"http://fotki.yandex.ru/users/loveangelus/view/1322703/\"&gt;&lt;img src=\"https://img-fotki.yandex.ru/get/15565/327456462.52/0_142ecf_c5b10b4d_M\" alt=\"C04O0322.jpg\" width=\"300\" height=\"200\"/&gt;&lt;/a&gt;&lt;br/&gt;&lt;/p&gt;</description>\n" +
            "      </item>\n" +
            "      <item>\n" +
            "         <author>makkar1961</author>\n" +
            "         <link>http://fotki.yandex.ru/users/makkar1961/view/1901328/</link>\n" +
            "         <media:thumbnail url=\"https://img-fotki.yandex.ru/get/4123/328427050.0/0_1d0310_3ac319a4_M\" medium=\"image\" type=\"image/jpeg\" />\n" +
            "         <media:content url=\"https://img-fotki.yandex.ru/get/4123/328427050.0/0_1d0310_3ac319a4_XL\" medium=\"image\" type=\"image/jpeg\" />\n" +
            "         <pubDate>Mon, 10 Aug 2015 01:16 +0300</pubDate>\n" +
            "         <title>.jpg</title>\n" +
            "         <description>&lt;p&gt;&lt;a href=\"http://fotki.yandex.ru/users/makkar1961/view/1901328/\"&gt;&lt;img src=\"https://img-fotki.yandex.ru/get/4123/328427050.0/0_1d0310_3ac319a4_M\" alt=\".jpg\" width=\"300\" height=\"200\"/&gt;&lt;/a&gt;&lt;br/&gt;&lt;/p&gt;</description>\n" +
            "      </item>" +
            "   </channel>\n" +
            "</rss>";
}