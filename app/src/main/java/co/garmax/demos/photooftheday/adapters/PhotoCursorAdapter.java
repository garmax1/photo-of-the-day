package co.garmax.demos.photooftheday.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;

import co.garmax.demos.photooftheday.data.PhotoDatabaseHelper;
import co.garmax.demos.photooftheday.loaders.ImageDownloadManager;
import co.garmax.demos.photooftheday.ui.views.ThumbnailView;

public class PhotoCursorAdapter extends CursorAdapter {

    private ImageDownloadManager mImageDownloadManager;

    public PhotoCursorAdapter(Context context, Cursor cursor) {
        super(context, cursor, 0);

        mImageDownloadManager = ImageDownloadManager.getInstance(context);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        return new ThumbnailView(context);

    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ThumbnailView viewPhoto = (ThumbnailView) view;

        // Get url
        String url = cursor.getString(cursor.getColumnIndexOrThrow(PhotoDatabaseHelper.COLUMN_URL));

        // Set task to load image to the view
        mImageDownloadManager.loadImage(viewPhoto, url);

    }
}