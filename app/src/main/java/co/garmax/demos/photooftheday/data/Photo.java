package co.garmax.demos.photooftheday.data;

import android.os.Parcel;
import android.os.Parcelable;

public class Photo implements Parcelable {
    private String mTitle;
    private String mAuthor;
    private String mUrl;

    public Photo(String title, String author, String url) {
        setTitle(title);
        setAuthor(author);
        setUrl(url);
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getAuthor() {
        return mAuthor;
    }

    public void setAuthor(String author) {
        this.mAuthor = author;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        this.mUrl = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mTitle);
        dest.writeString(mAuthor);
        dest.writeString(mUrl);
    }

    public static final Parcelable.Creator<Photo> CREATOR
            = new Parcelable.Creator<Photo>() {
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    private Photo(Parcel in) {
        mTitle = in.readString();
        mAuthor = in.readString();
        mUrl = in.readString();
    }
}
