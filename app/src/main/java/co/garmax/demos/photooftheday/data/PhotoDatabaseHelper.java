package co.garmax.demos.photooftheday.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PhotoDatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = PhotoDatabaseHelper.class.getSimpleName();

    private static volatile PhotoDatabaseHelper sInstance;

    private static final String DATABASE_NAME = "photos.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TABLE_PHOTOS = "photos";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_AUTHOR = "author";
    public static final String COLUMN_URL = "url";

    public static PhotoDatabaseHelper getInstance(Context context) {
        PhotoDatabaseHelper localInstance = sInstance;
        if (localInstance == null) {
            synchronized (PhotoDatabaseHelper.class) {
                localInstance = sInstance;
                if (localInstance == null) {
                    sInstance = localInstance = new PhotoDatabaseHelper(context);
                }
            }
        }
        return localInstance;
    }

    PhotoDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create tables
        db.execSQL("CREATE TABLE " + TABLE_PHOTOS + " ("
                + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_TITLE + " TEXT, "
                + COLUMN_AUTHOR + " TEXT, "
                + COLUMN_URL + " TEXT UNIQUE);");

        // Create indexec
        db.execSQL("CREATE INDEX photoIndexId ON " + TABLE_PHOTOS + "(" + COLUMN_ID + ");");
        db.execSQL("CREATE INDEX photoIndexURL ON " + TABLE_PHOTOS + "(" + COLUMN_URL + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to " +
                newVersion + ", which will destroy all old data");

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PHOTOS);
        onCreate(db);
    }

    public static long insertPhoto(SQLiteDatabase db, Photo photo) {
        // Set values
        ContentValues values = new ContentValues();
        values.put(COLUMN_TITLE, photo.getTitle());
        values.put(COLUMN_AUTHOR, photo.getAuthor());
        values.put(COLUMN_URL, photo.getUrl());

        // Insert photo and return new id
        return db.insertWithOnConflict(TABLE_PHOTOS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
    }

    // Return all photos sorted by date
    public Cursor getPhotoCursor() {

        SQLiteDatabase db = getReadableDatabase();

        return db.query(TABLE_PHOTOS, new String[]{COLUMN_ID, COLUMN_TITLE, COLUMN_AUTHOR, COLUMN_URL},
                null, null, null, null, null);
    }
}
