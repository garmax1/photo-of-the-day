package co.garmax.demos.photooftheday.data;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class PhotoXmlParser {

    private static final String NAMESPACE_NULL = null;

    // XML tags
    private static final String XML_TAG_ITEM = "item";
    private static final String XML_TAG_AUTHOR = "author";
    private static final String XML_TAG_TITLE = "title";
    private static final String XML_TAG_URL = "media:content";
    private static final String XML_TAG_CHANNEL = "channel";
    private static final String XML_TAG_RSS = "rss";
    private static final String XML_ATTRIBUTE_URL = "url";

    public List<Photo> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, null);

            // Skip rss node
            parser.nextTag();
            parser.require(XmlPullParser.START_TAG, NAMESPACE_NULL, XML_TAG_RSS);

            // Skip channel node
            parser.nextTag();
            parser.require(XmlPullParser.START_TAG, NAMESPACE_NULL, XML_TAG_CHANNEL);

            return readPhotos(parser);
        } finally {
            in.close();
        }
    }

    private List<Photo> readPhotos(XmlPullParser parser) throws XmlPullParserException, IOException {
        List<Photo> photosList = new ArrayList<>();

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the photo tag
            if (name.equals(XML_TAG_ITEM)) {
                photosList.add(readPhoto(parser));
            } else {
                skip(parser);
            }
        }
        return photosList;
    }

    private Photo readPhoto(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE_NULL, XML_TAG_ITEM);
        String title = null;
        String author = null;
        String url = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            switch (name) {
                case XML_TAG_TITLE:
                    title = readTitle(parser);
                    break;
                case XML_TAG_AUTHOR:
                    author = readAuthor(parser);
                    break;
                case XML_TAG_URL:
                    url = readUrl(parser);
                    break;
                default:
                    skip(parser);
                    break;
            }
        }
        return new Photo(title, author, url);
    }

    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE_NULL, XML_TAG_TITLE);
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, NAMESPACE_NULL, XML_TAG_TITLE);
        return title;
    }

    private String readAuthor(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE_NULL, XML_TAG_AUTHOR);
        String author = readText(parser);
        parser.require(XmlPullParser.END_TAG, NAMESPACE_NULL, XML_TAG_AUTHOR);
        return author;
    }

    private String readUrl(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, NAMESPACE_NULL, XML_TAG_URL);
        String url = parser.getAttributeValue(NAMESPACE_NULL, XML_ATTRIBUTE_URL);
        parser.nextTag();
        parser.require(XmlPullParser.END_TAG, NAMESPACE_NULL, XML_TAG_URL);
        return url;
    }

    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
