package co.garmax.demos.photooftheday.loaders;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.graphics.BitmapCompat;
import android.support.v4.util.LruCache;
import android.view.View;

import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import co.garmax.demos.photooftheday.ui.views.ThumbnailView;

public class ImageDownloadManager {

    static volatile ImageDownloadManager sInstance = null;

    private Context mContext;

    // Bitmap cache in the memory
    private LruCache<String, Bitmap> mThumbnailCache;

    // Limited thread pool by core count
    private ExecutorService mThreadPoolExecutor;

    // Loading tasks
    private WeakHashMap<View, Future> mQueueForLoading;

    // Used to update ImageView in main thread
    private Handler mMainHandler;

    public static ImageDownloadManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (ImageDownloadManager.class) {
                if (sInstance == null) {
                    sInstance = new ImageDownloadManager(context);
                }
            }
        }
        return sInstance;
    }

    public ImageDownloadManager(Context context) {
        // Use app context to prevent usage of not stable activity context
        mContext = context.getApplicationContext();

        // Processor cores
        int threads = Runtime.getRuntime().availableProcessors();

        mThreadPoolExecutor = Executors.newFixedThreadPool(threads);

        mQueueForLoading = new WeakHashMap<>();

        mMainHandler = new Handler(Looper.getMainLooper());

        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Use 50% of heap size
        int cacheSize = Math.round(Runtime.getRuntime().maxMemory() / 2);
        mThumbnailCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return BitmapCompat.getAllocationByteCount(value);
            }
        };
    }

    public void loadImage(ThumbnailView view, String url) {
        // Set url associated with imageview,
        view.setTag(url);

        // Reset previous task if exist and remove from queue
        if(mQueueForLoading.containsKey(view)) {
            mQueueForLoading.get(view).cancel(true);
            mQueueForLoading.remove(view);
        }

        // Get image from cache
        Bitmap thumbnail = getThumbnailFromCache(url);

        // Set image from the cache if exist
        if (thumbnail != null) {
            view.setImage(thumbnail);
        }
        // Load image and create thumbnail
        else {
            // Set loading background
            view.setProgress();

            // Start loading image
            addNewDownloadTask(view, url);
        }
    }

    public Bitmap getThumbnailFromCache(String url) {
        return mThumbnailCache.get(url);
    }

    public Bitmap putThumbnailToCache(String url, Bitmap bitmap) {
        return mThumbnailCache.put(url, bitmap);
    }

    private void addNewDownloadTask(ThumbnailView view, String url) {
        // Add new task to the thread pool
        Future future = mThreadPoolExecutor.submit(new ImageDownloadTask(this, view, url));

        // Add Future to the list of tasks
        mQueueForLoading.put(view, future);
    }

    public Context getContext() {
        return mContext;
    }

    public Handler getMainHandler() {
        return mMainHandler;
    }
}
