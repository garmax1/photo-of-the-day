package co.garmax.demos.photooftheday.loaders;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.webkit.URLUtil;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

import co.garmax.demos.photooftheday.R;
import co.garmax.demos.photooftheday.ui.views.ThumbnailView;
import co.garmax.demos.photooftheday.utils.NetworkUtils;

/**
 * This class download image if not exist and create thumbnail
 * and update imageview
 */
public class ImageDownloadTask implements Runnable {
    private static final String TAG = ImageDownloadTask.class.getSimpleName();
    private static final String TEMP_SUFFIX = "__temp";
    private static final String THUMBNAIL_SUFFIX = "__thumb";

    private WeakReference<ThumbnailView> mViewReference;

    private final String mUrl;

    private ImageDownloadManager mImageDownloadManager;

    public ImageDownloadTask(ImageDownloadManager manager, ThumbnailView view, String url) {

        mViewReference = new WeakReference<>(view);

        mUrl = url;

        mImageDownloadManager = manager;

    }

    @Override
    public void run() {
        Context context = mImageDownloadManager.getContext();

        File thumbnailFile = new File(getImagePath(context, mUrl) + THUMBNAIL_SUFFIX);
        Bitmap thumbnail = null;
        final ThumbnailView thumbnailView = mViewReference.get();

        //Check thumbnail file on sdcard
        if (thumbnailFile.exists()) {
            try {
                thumbnail = BitmapFactory.decodeFile(thumbnailFile.getPath());
            } catch (OutOfMemoryError e) {
                Log.e(TAG, "Exception when decode thumbnail file", e);
            }
        }
        // We should download image if needed and create thumbnail file
        else {
            thumbnail = getThumbnail(mImageDownloadManager.getContext(), mUrl,
                    thumbnailView.getWidth(), thumbnailView.getHeight());
        }

        // Exit if can't create thumbnail
        if (thumbnail == null) {
            if (thumbnailView != null && mUrl.equals(thumbnailView.getTag())) {
                mImageDownloadManager.getMainHandler().post(new Runnable() {
                    @Override
                    public void run() {
                        thumbnailView.setColor(
                                ContextCompat.getColor(mImageDownloadManager.getContext(),
                                R.color.fail_loaded_image));
                    }
                });
            }

            return;
        }

        // Add to the lrucache
        mImageDownloadManager.putThumbnailToCache(mUrl, thumbnail);

        // Check that imageview associated to the right url
        if (thumbnailView != null && mUrl.equals(thumbnailView.getTag())) {
            final Bitmap finalThumbnail = thumbnail;
            mImageDownloadManager.getMainHandler().post(new Runnable() {
                @Override
                public void run() {
                    thumbnailView.setImage(finalThumbnail);
                }
            });
        }
    }

    public static String getImagePath(Context context, String url) {
        return context.getExternalCacheDir() + File.separator +
                URLUtil.guessFileName(url, null, "image/jpeg");
    }

    private Bitmap getThumbnail(Context context, String url, int width, int height) {
        File imageFile = new File(getImagePath(mImageDownloadManager.getContext(), url));
        Bitmap thumbnail = null;
        Bitmap fullImage = null;

        // Download image
        if (!imageFile.exists()) {
            if (NetworkUtils.checkNetworkConnection(mImageDownloadManager.getContext())) {
                try {
                    fullImage = NetworkUtils.downloadBitmapFromServer(url);
                } catch (IOException e) {
                    Log.e(TAG, "Can't load image from the server");
                }
            }

            // Save bitmap to the file
            if (fullImage != null) {

                saveBitmap(fullImage, imageFile);

            }
        }
        // Read image from file
        else {
            try {
                fullImage = BitmapFactory.decodeFile(imageFile.getPath());
            } catch (OutOfMemoryError e) {
                Log.e(TAG, "Exception when decode full image file", e);
            }
        }

        // Create thumbnail
        if (fullImage != null) {
            thumbnail = ThumbnailUtils.extractThumbnail(fullImage, width, height);

            saveBitmap(thumbnail, new File(getImagePath(context, url) + THUMBNAIL_SUFFIX));

            // Release full bitmap
            fullImage.recycle();
        }

        return thumbnail;
    }

    public static boolean saveBitmap(Bitmap bitmap, File destFile) {
        // Try to save only if sdcard mounted
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            FileOutputStream fOut = null;
            try {
                // Use temp file because app can be stopped when we trying write bitmap
                File tempFile = new File(destFile.getPath() + TEMP_SUFFIX);

                fOut = new FileOutputStream(tempFile);

                bitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                fOut.flush();

                return tempFile.renameTo(destFile);
            } catch (FileNotFoundException e) {
                Log.e(TAG, "Exception when save bitmap to the file " + destFile.getPath(), e);
            } catch (IOException e) {
                Log.e(TAG, "Exception when write bitmap to the file " + destFile.getPath(), e);
            } finally {
                if (fOut != null) {
                    try {
                        fOut.close();
                    } catch (IOException e) {
                        Log.e(TAG, "Can't close output stream", e);
                    }
                }
            }
        }

        return false;
    }
}
