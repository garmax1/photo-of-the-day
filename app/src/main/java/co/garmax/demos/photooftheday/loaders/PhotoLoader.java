package co.garmax.demos.photooftheday.loaders;

import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import co.garmax.demos.photooftheday.data.Photo;
import co.garmax.demos.photooftheday.data.PhotoDatabaseHelper;
import co.garmax.demos.photooftheday.data.PhotoXmlParser;
import co.garmax.demos.photooftheday.utils.NetworkUtils;

/**
 * Load photos data from the server and write to the database
 * Return true if if got new photos
 */
public class PhotoLoader extends AsyncTaskLoader<Integer> {
    private static final String TAG = PhotoLoader.class.getSimpleName();

    public static final int RESULT_EMPTY = 0;
    public static final int RESULT_NEW_PHOTOS = 1;
    public static final int RESULT_ERROR = 2;

    public PhotoLoader(Context context) {
        super(context);
    }

    @Override
    public Integer loadInBackground() {
        int iResult = RESULT_EMPTY;

        // Load photos from the server
        List<Photo> photoListServer = new ArrayList<>();

        try {
            // Get stream for reading photos
            InputStream inputStream = NetworkUtils.getUrlInputStream(NetworkUtils.URL_PHOTOS);

            // Read and parse photos from the server
            photoListServer = new PhotoXmlParser().parse(inputStream);

        } catch (IOException e) {

            Log.e(TAG, "Exception when load photos from the server", e);

        } catch (XmlPullParserException e) {

            Log.e(TAG, "Exception when parse photos from the response", e);

        }

        SQLiteDatabase writableDatabase = PhotoDatabaseHelper.getInstance(getContext()).getWritableDatabase();
        writableDatabase.beginTransaction();

        // Write new photos to the database
        if (photoListServer != null && !photoListServer.isEmpty()) {
            try {
                for (Photo photo : photoListServer) {
                    // Write photo
                    long id = PhotoDatabaseHelper.insertPhoto(writableDatabase, photo);

                    // If id negative it's mean that photo already in the database
                    if (id >= 0) {
                        iResult = RESULT_NEW_PHOTOS;
                    }
                }

                writableDatabase.setTransactionSuccessful();
            } finally {
                writableDatabase.endTransaction();
                writableDatabase.close();
            }
        }
        // Can't load photos from the server
        else {
            iResult = RESULT_ERROR;
        }

        return iResult;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();

        forceLoad();
    }

    @Override
    protected void onStopLoading() {
        super.onStopLoading();

        cancelLoad();
    }

}
