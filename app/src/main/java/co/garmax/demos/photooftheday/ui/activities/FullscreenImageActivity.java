package co.garmax.demos.photooftheday.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import co.garmax.demos.photooftheday.R;
import co.garmax.demos.photooftheday.loaders.ImageDownloadTask;

public class FullscreenImageActivity extends AppCompatActivity {
    private static final String TAG = FullscreenImageActivity.class.getSimpleName();

    private static final String EXTRA_TITLE = "extra_title";
    private static final String EXTRA_AUTHOR = "extra_author";
    private static final String EXTRA_URL = "extra_url";

    private ImageView mImagePhoto;
    private TextView mTextDescription;

    public static void startActivity(Context context, String url, String title, String author) {
        Intent intent = new Intent(context, FullscreenImageActivity.class);
        intent.putExtra(EXTRA_URL, url);
        intent.putExtra(EXTRA_TITLE, title);
        intent.putExtra(EXTRA_AUTHOR, author);
        context.startActivity(intent);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(createView(this));

        // Initialize views
        setUpViews();
    }

    /**
     * Generate activity view programmatically, because we have this condition in the task
     */
    private View createView(Context context) {
        FrameLayout parentLayout = new FrameLayout(context);
        mImagePhoto = new ImageView(context);
        mTextDescription = new TextView(context);

        // Image
        FrameLayout.LayoutParams imageParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        parentLayout.addView(mImagePhoto, imageParams);

        // Description
        FrameLayout.LayoutParams textDescriptionParams = new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        textDescriptionParams.gravity = Gravity.CENTER | Gravity.BOTTOM;
        mTextDescription.setGravity(Gravity.CENTER);
        parentLayout.addView(mTextDescription, textDescriptionParams);

        return parentLayout;
    }

    private void setUpViews() {

        // Get photo instance
        Intent intent = getIntent();
        String url = intent.getStringExtra(EXTRA_URL);
        String author = intent.getStringExtra(EXTRA_AUTHOR);
        String title = intent.getStringExtra(EXTRA_TITLE);

        // Set data
        mTextDescription.setText(author + System.getProperty("line.separator") + title);
        mTextDescription.setBackgroundColor(ContextCompat.getColor(this, R.color.fullscreen_panel_background));
        mTextDescription.setTextColor(ContextCompat.getColor(this, R.color.fullscreen_panel_text));

        // Load from file if exists
        String imagePath = ImageDownloadTask.getImagePath(this, url);

        Bitmap bitmap = BitmapFactory.decodeFile(imagePath);

        if(bitmap != null) {
            mImagePhoto.setImageBitmap(bitmap);
        } else {
            Log.e(TAG, "Can't load image file " + imagePath);
        }
    }
}
