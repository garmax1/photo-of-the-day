package co.garmax.demos.photooftheday.ui.activities;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import co.garmax.demos.photooftheday.R;
import co.garmax.demos.photooftheday.adapters.PhotoCursorAdapter;
import co.garmax.demos.photooftheday.data.PhotoDatabaseHelper;
import co.garmax.demos.photooftheday.loaders.PhotoLoader;

public class GridActivity extends AppCompatActivity implements
        LoaderManager.LoaderCallbacks<Integer>, SwipeRefreshLayout.OnRefreshListener {
    private static final String TAG = GridActivity.class.getSimpleName();

    private static final int LOADER_PHOTO_ID = 0;

    private TextView mTextEmptyGrid;
    private SwipeRefreshLayout mSwipeOrderList;
    private SwipeRefreshLayout mSwipeEmptyListView;
    private GridView mGridView;

    // This is the Adapter being used to display the list's data.
    private PhotoCursorAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(createView(this));

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) mAdapter.getItem(position);
                String url = cursor.getString(
                        cursor.getColumnIndex(PhotoDatabaseHelper.COLUMN_URL));
                String author = cursor.getString(
                        cursor.getColumnIndex(PhotoDatabaseHelper.COLUMN_AUTHOR));
                String title = cursor.getString(
                        cursor.getColumnIndex(PhotoDatabaseHelper.COLUMN_TITLE));

                // Start fullscreen image activity
                FullscreenImageActivity.startActivity(GridActivity.this, url, title, author);
            }
        });
        mSwipeOrderList.setOnRefreshListener(this);
        mSwipeEmptyListView.setOnRefreshListener(this);

        // Create new apdater on first start
        // we used retained fragment after recreation adapter won't destroyed

        mAdapter = new PhotoCursorAdapter(this,
                PhotoDatabaseHelper.getInstance(this).getPhotoCursor());
        mGridView.setAdapter(mAdapter);

        checkEmptyListView();

        // Start loader if first
        if(savedInstanceState == null) {
            getSupportLoaderManager().initLoader(LOADER_PHOTO_ID, null, this);
        }
    }

    /**
     * Generate activity view programmatically, because we have this condition in the task
     */
    public View createView(Context context) {
        FrameLayout parentLayout = new FrameLayout(context);
        ScrollView scrollView = new ScrollView(context);
        mTextEmptyGrid= new TextView(context);
        mSwipeEmptyListView = new SwipeRefreshLayout(context);
        mSwipeOrderList = new SwipeRefreshLayout(context);
        mGridView = new GridView(context);

        // Empty view for swipe to refresh
        mTextEmptyGrid.setGravity(Gravity.CENTER);
        ScrollView.LayoutParams textEmptyParams = new ScrollView.LayoutParams(
                ScrollView.LayoutParams.MATCH_PARENT, ScrollView.LayoutParams.MATCH_PARENT);
        textEmptyParams.gravity = Gravity.CENTER;
        scrollView.addView(mTextEmptyGrid, textEmptyParams);
        mSwipeEmptyListView.addView(scrollView);

        // Grid view swipe to refresh
        mGridView.setClipToPadding(true);
        mGridView.setStretchMode(GridView.STRETCH_COLUMN_WIDTH);
        mGridView.setNumColumns(getResources().getInteger(R.integer.grid_column_count));
        int spacing = getResources().getDimensionPixelSize(R.dimen.grid_cell_space);
        mGridView.setHorizontalSpacing(spacing);
        mGridView.setVerticalSpacing(spacing);
        mSwipeOrderList.addView(mGridView);

        parentLayout.addView(mSwipeOrderList);
        parentLayout.addView(mSwipeEmptyListView);

        return parentLayout;
    }

    public Loader<Integer> onCreateLoader(int id, Bundle args) {

        updateProgressBar(true);

        // Set message that will load photos
        setMessage(R.string.text_load_photos);

        return new PhotoLoader(this);
    }

    private void setMessage(int messageResId) {
        // Show warning if list is empty
        if (mAdapter.isEmpty()) {
            mTextEmptyGrid.setText(messageResId);
        }
    }

    @Override
    public void onLoadFinished(Loader<Integer> loader, Integer result) {
        getLoaderManager().destroyLoader(LOADER_PHOTO_ID);

        Log.d(TAG, "Load photos result " + result);

        updateProgressBar(false);

        // Notify that we have new photos
        if(result == PhotoLoader.RESULT_NEW_PHOTOS) {
            mAdapter.changeCursor(PhotoDatabaseHelper.getInstance(this).getPhotoCursor());

            checkEmptyListView();
        } else if(result == PhotoLoader.RESULT_ERROR) {

            setMessage(R.string.text_list_empty);

            Toast.makeText(this, R.string.toast_server_connection_error, Toast.LENGTH_SHORT).show();
        }
    }

    public void onLoaderReset(Loader<Integer> loader) {
    }

    /**
     * Show or hide empty view of the list
     */
    private void checkEmptyListView() {
        if (!mAdapter.isEmpty()) {
            mSwipeEmptyListView.setRefreshing(false);
            mSwipeEmptyListView.setVisibility(View.GONE);
            mSwipeOrderList.setVisibility(View.VISIBLE);
        } else {
            mSwipeOrderList.setRefreshing(false);
            mSwipeOrderList.setVisibility(View.GONE);
            mSwipeEmptyListView.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Show or hide progress bar
     */
    private void updateProgressBar(final boolean show) {

        if (mAdapter.isEmpty()) {
            // Use post to prevent bug when load icon not showed
            mSwipeEmptyListView.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeEmptyListView.setRefreshing(show);
                }
            });
        } else {
            // Use post to prevent bug when load icon not showed
            mSwipeOrderList.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeOrderList.setRefreshing(show);
                }
            });
        }
    }

    @Override
    public void onRefresh() {
        getSupportLoaderManager().initLoader(LOADER_PHOTO_ID, null, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // Reset loader to finish him immediatelly if in progress
        Loader loader = getSupportLoaderManager().getLoader(LOADER_PHOTO_ID);
        if (loader != null) {
            loader.reset();
        }
    }
}
