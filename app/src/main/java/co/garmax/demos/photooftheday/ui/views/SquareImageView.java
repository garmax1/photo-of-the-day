package co.garmax.demos.photooftheday.ui.views;

import android.content.Context;
import android.widget.ImageView;

public class SquareImageView extends ImageView{
    public SquareImageView(Context context) {
        super(context);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        // Make height as width
        int size = getMeasuredWidth();
        setMeasuredDimension(size, size);
    }

}
