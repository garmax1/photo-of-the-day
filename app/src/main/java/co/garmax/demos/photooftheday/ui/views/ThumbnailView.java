package co.garmax.demos.photooftheday.ui.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

public class ThumbnailView extends FrameLayout {
    // Show progress if image loading
    ProgressBar mProgressBar;

    // Show image
    private SquareImageView mImageView;

    public ThumbnailView(Context context) {
        super(context);

        init();
    }

    public ThumbnailView(Context context, AttributeSet attrs) {
        super(context, attrs);

        init();
    }

    public ThumbnailView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init();
    }

    private void init() {
        // Add progress
        mProgressBar = new ProgressBar(getContext());
        LayoutParams progressParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        progressParams.gravity = Gravity.CENTER;
        addView(mProgressBar, progressParams);

        // Add image
        mImageView = new SquareImageView(getContext());
        LayoutParams imageParams = new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT);

        addView(mImageView, imageParams);
    }

    public void setProgress() {
        mProgressBar.setVisibility(View.VISIBLE);

        mImageView.setVisibility(View.INVISIBLE);
        mImageView.setImageDrawable(null);

        setBackgroundColor(Color.TRANSPARENT);
    }

    public void setImage(Bitmap bitmap) {
        mProgressBar.setVisibility(View.INVISIBLE);

        mImageView.setVisibility(View.VISIBLE);
        mImageView.setImageBitmap(bitmap);

        setBackgroundColor(Color.TRANSPARENT);
    }

    public void setColor(int color) {
        mProgressBar.setVisibility(View.INVISIBLE);

        mImageView.setVisibility(View.INVISIBLE);
        mImageView.setImageDrawable(null);

        setBackgroundColor(color);
    }
}
