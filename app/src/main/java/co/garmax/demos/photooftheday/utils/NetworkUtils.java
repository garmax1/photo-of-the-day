package co.garmax.demos.photooftheday.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class NetworkUtils {
    public static final String URL_PHOTOS = "http://fotki.yandex.ru/calendar/rss2";

    private static final int TIMEOUT_READ = 10000;
    private static final int TIMEOUT_CONNECT = 15000;

    public static InputStream getUrlInputStream(String urlString) throws IOException {

        URL url = new URL(urlString);

        // Set connection
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setReadTimeout(TIMEOUT_READ);
        conn.setConnectTimeout(TIMEOUT_CONNECT);
        conn.setRequestMethod("GET");
        conn.setDoInput(true);

        // Execute connection
        conn.connect();

        return conn.getInputStream();
    }

    public static Bitmap downloadBitmapFromServer(String imageUrl) throws IOException {
        InputStream inputStream = getUrlInputStream(imageUrl);
        return BitmapFactory.decodeStream(inputStream);
    }

    public static boolean checkNetworkConnection(Context context) {
        // Check network connection
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        return netInfo != null && netInfo.isConnected();
    }
}
